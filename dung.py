import random as randy
import pygame, sys
from pygame.locals import *
from pygame import Color as rgb


# Colores        (R)  (G)  (B)
WHITE      = rgb(235, 235, 235)
BLACK      = rgb( 13,  13,  13)
CORRIDOR =   rgb(102, 205, 170)
ROOM  =      rgb(205, 102, 137)

# Constantes del entorno que definen el mundo
BLOCKSIZE = 20
SCREENX = 400
SCREENY = 300

pygame.init()
fontObj = pygame.font.Font('CubicCoreMono.ttf', 28)
clock = pygame.time.Clock()
DISPLAYSURF = pygame.display.set_mode((SCREENX, SCREENY))
pygame.display.set_caption('Dungeon generator')
randy.seed(hash("panete"))

print("Semilla panete: ", hash("panete"))

# Considerar 0 como solido, los demas numeros como numeros de habitación

def main():
    """
    Generamos una mazmorra de prueba cuando este modulo no se usa como una libreria
    """
    dungeon, rooms = generate_dungeon()
    print(len(dungeon), " x ", len(dungeon[0]))
    print(rooms)

    while True:
        """La parte encargada de ejecutar los bucles del juego en un orden que
        hace la animacion sincronizada.
        """
        pygame.display.update()
        control()
        show_mapa(dungeon, rooms)
        clock.tick(24)

def show_mapa(mapa, habs):
    DISPLAYSURF.fill(WHITE)
    for x in range(0, len(mapa)):
        for y in range(0, len(mapa[x])):
            if not (mapa[x][y] == 0):
                pygame.draw.rect(DISPLAYSURF, ROOM, [x*BLOCKSIZE, y*BLOCKSIZE, BLOCKSIZE, BLOCKSIZE])
            else:
                pass

    for key, value in habs.items():
        txtSurface = fontObj.render(str(key), True, BLACK)
        txtRectSurface = txtSurface.get_rect()
        txtRectSurface.center = (value[0]*BLOCKSIZE+BLOCKSIZE/2, value[1]*BLOCKSIZE+BLOCKSIZE/2)
        DISPLAYSURF.blit(txtSurface, txtRectSurface)


def control():
    """ems pues eso, salimos
    """
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()


# Variables controlables desde la iu como argumentos
def generate_dungeon(min_room_h = 2, max_room_h = 5, min_room_w = 2,
max_room_w = 5, min_room_n = 2, max_room_n = 9, max_attemps_fill = 250,
total_h = 20, total_w = 15): # total_h = ancho, total_w = alto corregir
    """
    Este metodo genera una matriz rellena de habitaciones.
    Se recomienda usar un min_room_n bajo respecto al tamañano mapa/habitacion:
    Mientras no tengamos el maximo de habitaciones ni hemos superado el numero
    de intentos por rellenar, o mientras no tengamos el minimo de habitaciones
    seguiremos intentando añadir una habitacion a la matriz, es posible
    terminar en un problema irresoluble cuando no sea posible meter el
    numero minimo indicado de habitaciones al mapa, por lo que se recomienda
    mantener bajo el numero minimo de habitaciones y alto el numero de intentos
    """
    # Variables por cada generación, que no deben ser accedidas por el usuario.
    rooms_now = 0
    attemps_now = 0
    mapa = [[0] * total_w for i in range(total_h)]
    rooms = {}
    while ((rooms_now < max_room_n and attemps_now < max_attemps_fill) or rooms_now < min_room_n):
        check_point_size = [randy.randint(min_room_h, max_room_h), randy.randint(min_room_w, max_room_w)]
        check_size = [x+2 for x in check_point_size]
        check_point = [randy.randint(0, total_h-check_size[0]), randy.randint(0, total_w-check_size[1])]
        if check_zeros(at=check_point, to=check_size, mapa=mapa):
            rooms[rooms_now+1] = list(x+1 for x in check_point)+list(check_point_size)
            mapa = fill_map(at=[x+1 for x in check_point], to=check_point_size, mapa=mapa, room=rooms_now+1)
            rooms_now += 1
        attemps_now += 1
    # Aqui se generan los pasillos
    mapa = generate_corridors(mapa, rooms)
    return mapa, rooms

def generate_corridors(mapa, rooms):
    """
    Este metodo se encarga de generar las adyacencias entre los nodos del "grafo"
    funciona de tal manera que añade las adyacencias minimas necesarias para
    que el grafo sea conexo, y ademas añade conexiones adicionales para
    asegurarse mas de un posible recorrido.Recibe un mapa y un diccionario
    de las habitaciones Devuelve el mapa con los pasillos añadidos.
    """

    copia_mapa = mapa
    real_adj = []
    aux_adj = []
    adj = []

    for x in range(len(rooms)-1):
        adj.append([])
        for y in range(x+1, len(rooms)):
            adj[x].append([x, y])

    for nodo in adj:
        chois = randy.choice(nodo)
        real_adj.append(chois)

    for nodo in adj:
        aux_adj.extend(nodo)

    objects = round(len(adj)/5)
    for x in range(0, objects):
        chois = randy.choice(aux_adj)
        real_adj.append(chois)
        aux_adj.remove(chois)
    print(real_adj)

    # Y por aqui decidimos los pasillos en si y los añadimos al mapa.

    return copia_mapa

def check_zeros(at, to, mapa):
    """Esta funcion comprueba si las casillas en las que intentamos poner
    la habitación, estan libres o no, en el peor caso estaran todas libres y
    tendremos que recorrer una submatriz de hasta max_room_h x max_room_w"""
    for x in range(at[0], at[0]+to[0]):
        for y in range(at[1], at[1]+to[1]):
            if not (mapa[x][y] == 0):
                return False
    return True

def fill_map(at, to, mapa, room):
    """Esta funcion rellena la submatriz seleccionada con el numero de
    habitacion que se nos proportciona, devuelve el mapa al que ha
    añadido la nueva submatriz, es decir, la habitación"""
    copia_mapa = mapa
    for x in range(at[0], at[0]+to[0]):
        for y in range(at[1], at[1]+to[1]):
            copia_mapa[x][y] = room
    return copia_mapa

def pixel(x, y, color):
    pass

if __name__ == '__main__':
    main()
