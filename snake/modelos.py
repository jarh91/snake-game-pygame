import pygame
from random import randint

pygame.init()
fontObj = pygame.font.Font('CubicCoreMono.ttf', 28)

def move_one(head, tail):
    """Esta funcion es una funcion de utilidad, pasa todos los elementos
    una posicion hacia atras y el primero lo convierte en el valor de head."""
    for x in reversed(range(1, len(tail))):
        tail[x] = tail[x-1]
    tail[0] = head
    return tail

class Snake():
    """La serpiente, la inicializamos con una pequeña cola detras."""
    def __init__(self, block, color):
        self.x = 0
        self.y = 0
        self.x_speed = 1
        self.y_speed = 0
        self.tail_len = 5
        self.tail = [(-1,0),(-2,0),(-3,0),(-4,0),(-5,0)]
        self.block = block
        self.color = color
        self.score = 0
        self.txtSurface = fontObj.render("score: {}".format(self.score), True, self.color)
        self.txtRectSurface = self.txtSurface.get_rect()
        self.txtRectSurface.center = (45, 12)

    def update(self, boundx, boundy, foodx, foody):
        """Comprobamos si la serpiente ha chocado o no, actualizamos
        su posicion, la hacemos crecer y etc funciones vitales."""

        self.txtSurface = fontObj.render("score: {}".format(self.score), True, self.color)

        if self.tail_len > 0:
            self.tail = move_one((self.x, self.y), self.tail)

        if self.y >= boundy and self.y_speed == 1 or self.y <= 0 and self.y_speed == -1:
            pass
        else:
            self.y += self.y_speed

        if self.x >= boundx and self.x_speed == 1 or self.x <= 0 and self.x_speed == -1:
            pass
        else:
            self.x += self.x_speed

        if foodx == self.x and foody == self.y:
            self.grow()

    def grow(self):
        """Agregamos un uno a la cola  de nuestra serpiente"""
        self.tail_len += 1
        self.tail.append((self.x, self.y))
        self.score+=10

    def control(self, key):
        """en caso de que se presiona una tecla comprobamos
        si son las que controlan a la serpiente, y segun la direccion
        en la que esta se dirige, actualizamos su nueva direccion"""
        if self.x_speed == 0:
            if key == pygame.K_LEFT:
                self.x_speed = -1
                self.y_speed = 0
            if key == pygame.K_RIGHT:
                self.x_speed = 1
                self.y_speed = 0

        if self.y_speed == 0:
            if key == pygame.K_UP:
                self.y_speed = -1
                self.x_speed = 0
            if key == pygame.K_DOWN:
                self.y_speed = 1
                self.x_speed = 0

    def show(self, screen):
        """dibujamos la serpiente como un conjunto de bloques, es este caso
        la cabeza es mas oscura que el resto de esta"""
        for piece in self.tail:
            pygame.draw.rect(screen, self.color, [piece[0]*self.block+1, piece[1]*self.block+1, self.block-2, self.block-2])
        pygame.draw.rect(screen, tuple(map(lambda x:x-35, self.color)), [self.x*self.block, self.y*self.block, self.block, self.block])
        screen.blit(self.txtSurface, self.txtRectSurface)


class Food():

    def __init__(self, color, width, boundx, boundy):
        self.color = color
        self.width = width
        self.boundx = boundx
        self.boundy = boundy
        self.x = randint(0,self.boundx)
        self.y = randint(0,self.boundy)

    def update(self, x, y):
        """Si la comida se ha comido, reiniciamos su posición
        (una mejora seria, comprobar que no aparezca dentro de la serpiente,
        ni donde estaba antes - que pasa a estar dentro de la serpiente en
        caso de que se lo come).
        """
        if self.x == x and self.y == y:
            self.x = randint(0,self.boundx)
            self.y = randint(0,self.boundy)

    def show(self, screen):
        """dibuja la comida como un "circulo feo rojo irregular con
        esquinas de sierra, la fincion circle de pygame, no funciona
        como se describe en la documentación.
        """
        #pygame.draw.rect(screen, self.color, [self.x*self.width, self.y*self.width, self.width, self.width])
        pygame.draw.ellipse(screen, self.color, [self.x*self.width, self.y*self.width, self.width, self.width] )
