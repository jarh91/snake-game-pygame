import pygame, sys
from pygame.locals import *
from pygame import Color as rgb
from modelos import Snake, Food

# Colores        (R)  (G)  (B)
WHITE      = rgb(235, 235, 235)
BLACK      = rgb( 10,  10,  10)
SNAKECOLOR = rgb(102, 205, 170)
FOODCOLOR  = rgb(205, 102, 137)

# Constantes del entorno que definen el mundo
BLOCKSIZE = 10
SCREENX = 400
SCREENY = 300
BOUNDX = SCREENX / BLOCKSIZE -1
BOUNDY = SCREENY / BLOCKSIZE -1

# Setup de la ventana:
pygame.init()
DISPLAYSURF = pygame.display.set_mode((SCREENX, SCREENY))
pygame.display.set_caption('Snake game')
# Creamos los objetos del juego:
clock = pygame.time.Clock()
snake = Snake(block=BLOCKSIZE, color=SNAKECOLOR)
food = Food(FOODCOLOR, BLOCKSIZE, BOUNDX, BOUNDY)

def update():
    """Realizamos el update de cada uno de nuestros objetos,
    lo ideal es que el update sea independiente para que los elementos
    sean reutilizables, aun que tengamos que pasarles valores adiciones.
    """
    snake.update(boundx = BOUNDX, boundy = BOUNDY, foodx = food.x, foody = food.y)
    food.update(snake.x, snake.y)

def control(moved):
    """Si lo que se presiona es una tecla, movemos una unica vez por tick, pero
    si el evento es de salir, cerramos la aplicación.
    """
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN and not moved:
            snake.control(event.key)
            moved = True
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

def show():
    """rellenamos la pantalla de blanco y luego dibujamos todos
    los elementos en nuestra pantalla, al dedicir crear los elementos
    reutilizables, les pasamos el display como argumento para dibujarlos
    sobre este de manera independiente.
    """
    DISPLAYSURF.fill(WHITE)
    snake.show(DISPLAYSURF)
    food.show(DISPLAYSURF)

while True:
    """La parte encargada de ejecutar los bucles del juego en un orden que
    hace la animacion sincronizada.
    """
    pygame.display.update()
    control(False)
    update()
    show()
    clock.tick(10.2) # velocidad del juego.
